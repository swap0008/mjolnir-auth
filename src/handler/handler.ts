import {
    loginController,
    signUpController,
    refreshTokenController,
    filterUserController,
    updateUserController
} from '../controller/controller.js';
import { applyMiddleware, verifyJwtToken } from 'middlewares-nodejs';

export const login = (event, context, callback) => {
    applyMiddleware(event, callback, [], loginController);
}

export const signUp = (event, context, callback) => {
    applyMiddleware(event, callback, [], signUpController);
}

export const healthCheck = (event, context, callback) => {
    const response = {
        statusCode: 200,
        body: JSON.stringify(`You're being served from ap-south-1 Mumbai region.`)
    }

    callback(null, response);
}

export const refreshToken = (event, context, callback) => {
    applyMiddleware(event, callback, [verifyJwtToken], refreshTokenController);
}

export const filterUser = (event, context, callback) => {
    applyMiddleware(event, callback, [verifyJwtToken], filterUserController);
}

export const testMiddleware = (event, context, callback) => {
    applyMiddleware(event, callback, [verifyJwtToken], (event, callback) => callback(null, { body: JSON.stringify(event) }));
}

export const updateUser = (event, context, callback) => {
    applyMiddleware(event, callback, [verifyJwtToken], updateUserController);
}