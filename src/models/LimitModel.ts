import { FirestoreClient } from './FirestoreClient.js';

export class LimitModel extends FirestoreClient {
    static NAME = 'name';
    
    constructor() {
        super('limits');
    }
}