import * as jwt from 'jsonwebtoken';
import { FirestoreClient } from './FirestoreClient.js';
import { JWT_PRIVATE_KEY } from '../constants/constants.js';

export class UserModel extends FirestoreClient {
    static NAME = 'name';
    static EMAIL = 'email';
    static PHONE = 'phone';
    static COUNTRY = 'country';
    static STATE = 'state';
    static CITY = 'city';
    static LANGUAGES = 'languages';
    static PRIMARY_DESTINATIONS = 'primary_destinations';
    static DEFAULT_CONTACTS_SYNC_TO = 'default_contacts_sync_to';
    static PASSWORD = 'password';
    static TYPE = 'type';
    static PLAN_TYPE = 'plan_type';
    static ROLE_IDS = 'role_ids';
    static LIMITS_ID = 'limits_id';
    static BUSINESS_IDS = 'business_ids';
    static IDENTIFIER = 'identifier';

    constructor() {
        super('users');
    }

    async getUserByIdentifierAndBusinessId(identifier: string, business_id: string) {
        const userDocs = await this.collectionRef
            .where(UserModel.IDENTIFIER, '==', identifier)
            .where(UserModel.BUSINESS_IDS, 'contains', business_id).get();
        
        return userDocs;
    }

    generateToken(data) {
        const token = jwt.sign(data, process.env[JWT_PRIVATE_KEY] || 'string');
        return token;
    }
}