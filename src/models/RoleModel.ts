import { FirestoreClient } from './FirestoreClient.js';

export class RoleModel extends FirestoreClient {
    static NAME = 'name';
    static DESCRIPTION = 'description';
    static PERMISSIONS = 'permissions';
    static ID = 'id';

    constructor() {
        super('roles');
    }
}