import { FirestoreClient } from './FirestoreClient.js';

export class PlanModel extends FirestoreClient {
    static NAME = 'name';
    static DESCRIPTION = 'description';
    static ROLE_ID = 'role_id';
    static LIMIT_ID = 'limit_id';
    static ACCOUNT_ID = 'account_id';

    constructor() {
        super('plans')
    }

    getPlans = () => {
        return ['BASIC', 'FREE', 'SMALL_TEAM', 'ENTERPRISE'];
    }
}