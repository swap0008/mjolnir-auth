import * as Joi from '@hapi/joi';
import { UserModel } from '../models/UserModel.js';

const getFields = () => {
    return {
        [UserModel.NAME]: Joi.string().pattern(/^[a-zA-Z ]+$/),
        [UserModel.EMAIL]: Joi.string().email(),
        [UserModel.PHONE]: Joi.number().integer(),
        [UserModel.COUNTRY]: Joi.string(),
        [UserModel.STATE]: Joi.string(),
        [UserModel.CITY]: Joi.string(),
        [UserModel.LANGUAGES]: Joi.array().unique(),
        [UserModel.PRIMARY_DESTINATIONS]: Joi.array().unique(),
        [UserModel.PASSWORD]: Joi.string(),
        [UserModel.PLAN_TYPE]: Joi.string(),
        [UserModel.TYPE]: Joi.string(),
        [UserModel.BUSINESS_IDS]: Joi.array().items(Joi.string().allow(''))
    };
};

export const loginSchema = (data) => {
    const fields = getFields();
    const schema = Joi.object({
        [UserModel.EMAIL]: fields[UserModel.EMAIL].required(),
        [UserModel.PASSWORD]: fields[UserModel.PASSWORD].required()
    });

    return schema.validate(data);
}

export const signUpSchema = (data) => {
    const fields = getFields();
    const schema = Joi.object({
        [UserModel.NAME]: fields[UserModel.NAME].required(),
        [UserModel.EMAIL]: fields[UserModel.EMAIL].required(),
        [UserModel.PHONE]: fields[UserModel.PHONE].required(),
        [UserModel.LANGUAGES]: fields[UserModel.LANGUAGES].required(),
        [UserModel.PASSWORD]: fields[UserModel.PASSWORD].required(),
        [UserModel.TYPE]: fields[UserModel.TYPE].allow(''),
        [UserModel.BUSINESS_IDS]: fields[UserModel.BUSINESS_IDS].required()
    });

    return schema.validate(data);
};

export const updateSchema = (data) => {
    const fields = getFields();
    const schema = Joi.object({
        [UserModel.NAME]: fields[UserModel.NAME],
        [UserModel.EMAIL]: fields[UserModel.EMAIL],
        [UserModel.PHONE]: fields[UserModel.PHONE],
        [UserModel.COUNTRY]: fields[UserModel.COUNTRY],
        [UserModel.STATE]: fields[UserModel.STATE],
        [UserModel.CITY]: fields[UserModel.CITY],
        [UserModel.LANGUAGES]: fields[UserModel.LANGUAGES],
        [UserModel.PRIMARY_DESTINATIONS]: fields[UserModel.PRIMARY_DESTINATIONS]
    });

    return schema.validate(data);
};