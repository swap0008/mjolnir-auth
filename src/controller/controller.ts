import { pick } from 'lodash';
import { UserModel } from '../models/UserModel.js';
import { RoleModel } from '../models/RoleModel.js';
import { PlanModel } from '../models/PlanModel.js';
import { LimitModel } from '../models/LimitModel.js';
import { signUpSchema, loginSchema } from '../schema/schema.js';

const User = new UserModel();
const Role = new RoleModel();
const Plan = new PlanModel();
const Limit = new LimitModel();

const response = (callback, statusCode, body) => {
    return callback(null, {
        statusCode: statusCode || 200,
        body: JSON.stringify(body)
    });
}

export const loginController = async (event, callback) => {
    const { email, password } = event.body;

    const { error } = loginSchema({ email, password });
    if (error) return response(callback, 400, error.details[0].message);

    const query = User.collectionRef.where(UserModel.EMAIL, '==', email).where(UserModel.PASSWORD, '==', password);
    const user = await query.get();

    if (user.empty) return response(callback, 400, 'Invalid email or password.');

    const userDoc = user.docs[0].data();

    let [roles, limits] = await Promise.all([
        Role.collectionRef.where(RoleModel.ID, 'in', userDoc[UserModel.ROLE_IDS]).get(),
        Limit.getDoc(userDoc[UserModel.LIMITS_ID])
    ]);

    let permissionsArray = [];
    roles.docs.forEach(doc => permissionsArray = permissionsArray.concat(doc.data().permissions));

    const permissions: any = {};
    permissionsArray.forEach(p => permissions[p] = true);

    limits = limits.data();

    const { business_ids, identifier = '' } = userDoc;

    const token = User.generateToken({
        user_id: email,
        business_ids,
        identifier,
        permissions,
        limits
    });

    response(callback, 200, { token });
}

export const signUpController = async (event, callback) => {
    const requestBody = event.body;

    const data = pick(requestBody, [
        UserModel.NAME,
        UserModel.EMAIL,
        UserModel.PHONE,
        UserModel.PASSWORD,
        UserModel.TYPE
    ]);

    data[UserModel.LANGUAGES] = ['en'];
    data[UserModel.BUSINESS_IDS] = [];
    if (!data[UserModel.TYPE]) data[UserModel.TYPE] = 'agency';

    const { error } = signUpSchema(data);
    if (error) return response(callback, 400, error.details[0].message);

    const { email, plan_type } = requestBody;
    const isExists = await User.collectionRef.where(UserModel.EMAIL, '==', email).get();
    if (!isExists.empty) return response(callback, 400, 'Email already registered.');

    const snapshot = await Plan.collectionRef.where(PlanModel.NAME, '==', plan_type).get();
    if (snapshot.empty) return response(callback, 400, 'No such plan exist!');

    const planData = snapshot.docs[0].data();

    data[UserModel.ROLE_IDS] = [planData[PlanModel.ROLE_ID]];
    data[UserModel.LIMITS_ID] = planData[PlanModel.LIMIT_ID];

    const { id } = User.collectionRef.doc();
    data[UserModel.IDENTIFIER] = id;

    await User.set(id, data);
    response(callback, 200, { id });
}

export const refreshTokenController = (event, callback) => {
    const { business_ids, user_id, permissions, limits, identifier } = event;
    const { business_id = '' } = event.pathParameters || {};

    if (!business_id) return response(callback, 400, 'Business id required!');

    const token = User.generateToken({
        user_id,
        business_ids: business_ids.concat(business_id),
        identifier,
        permissions,
        limits
    });

    response(callback, 200, { token });
}

export const filterUserController = async (event, callback) => {
    const { identifier } = event;
    const { business_id = '' } = event.pathParameters || {};

    if (!business_id) return response(callback, 400, 'Business id required!');

    if (!identifier) return response(callback, 403, 'Unauthorized.');

    const snapshot = await User.collectionRef.where(UserModel.BUSINESS_IDS, 'array-contains', business_id);
    if (snapshot.empty) return response(callback, 400, 'No user found');

    const users = snapshot.docs.map(doc => doc.data());
    response(callback, 200, users);
}

export const updateUserController = async (event, callback) => {
    const { user_id = '' } = event.pathParameters || {};
    const { identifier } = event;
    const { type, user_details, business_id } = event.body;

    let userDocs: any = {};

    switch (type) {
        case 'REMOVE_BUSINESS_ID':
            userDocs = await User.getUserByIdentifierAndBusinessId(identifier, business_id);
            if (userDocs.empty) return response(callback, 200, { status: 'updated' });
            await User.updateDocsArray(userDocs, UserModel.BUSINESS_IDS, business_id);
            return response(callback, 200, { status: 'updated' });
        case 'ADD_BUSINESS_ID':
            await User.updateArrayUnion(user_id, UserModel.BUSINESS_IDS, business_id);
            return response(callback, 200, { status: 'updated' });
        default:
            await User.update(user_id, user_details);
            response(callback, 200, 'User information updated.');
    }
}